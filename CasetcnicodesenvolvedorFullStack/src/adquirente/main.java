package adquirente;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class main {

	public static void main(String[] args) {

		String filename = "CasetcnicodesenvolvedorFullStack.txt";

		try (Stream<String> stream = Files.lines(Paths.get(filename), StandardCharsets.UTF_8)) {
			stream.forEach(linha -> {
				if (linha.substring(0, 1).equals("0")) {
					String tipoDeRegistro = linha.substring(0, 1);
					String estabelecimento = linha.substring(1, 11);
					String dataDeProcessamento = linha.substring(11, 19);
					String periodoInicial = linha.substring(19, 27);
					String periodoFinal = linha.substring(27, 35);
					String sequencia = linha.substring(35, 42);
					String empresaAdquirente = linha.substring(42, 47);
					String tipoDeExtrato = linha.substring(47, 49);
					String filler = linha.substring(49, 70);
					String versaoLayout = linha.substring(70, 73);
					String versaoRelease = linha.substring(73, 78);
					String reservadoParaUsofuturo = linha.substring(78);

					int tipoDeRegistroInt = strToInt(tipoDeRegistro);
					int estabelecimentoInt = strToInt(estabelecimento);
					int dataDeProcessamentoInt = strToInt(dataDeProcessamento);
					int periodoInicialInt = strToInt(periodoInicial);
					int periodoFinalInt = strToInt(periodoFinal);
					int sequenciaInt = strToInt(sequencia);
					int tipoDeExtratoInt = strToInt(tipoDeExtrato);

					System.out.printf(tipoDeRegistroInt + " " + estabelecimentoInt + " " + dataDeProcessamentoInt + " "
							+ periodoInicialInt + " " + periodoFinalInt + " " + sequenciaInt + " " + empresaAdquirente
							+ " " + tipoDeExtratoInt + " " + filler + " " + versaoLayout + " " + versaoRelease + " "
							+ reservadoParaUsofuturo + "\n");

				}

				if (linha.substring(0, 1).equals("1")) {
					String tipoDeRegistro = linha.substring(0, 1);
					String estabelecimento = linha.substring(1, 11);
					String dataInicialDaTransacao = linha.substring(11, 19);
					String datadoEvento = linha.substring(19, 27);
					String horaDoEvento = linha.substring(27, 33);
					String tipoDeEvento = linha.substring(33, 35);
					String tipoDeTransacao = linha.substring(35, 37);
					String numeroDeSerieDeLeitor = linha.substring(37, 45);
					String codigoDaTransacao = linha.substring(45, 77);
					String codigoDoPedido = linha.substring(77, 97);
					String valorTotalDaTransacao = linha.substring(97, 110);
					String valorDaParcelaOuLiquidoTotal = linha.substring(110, 123);
					String pagamento = linha.substring(123, 124);
					String plano = linha.substring(124, 126);
					String parcela = linha.substring(126, 128);
					String quantidaeDeParcelasDaTransacao = linha.substring(128, 130);
					String dataPrevistaDePagamento = linha.substring(130, 138);
					String taxaDeParcelamentoComprador = linha.substring(138, 151);
					String tarifaBoletoComprador = linha.substring(151, 164);
					String valorOriginalDaTransacao = linha.substring(164, 177);
					String taxaDeParcelamentoVendedor = linha.substring(177, 190);
					String taxaDeIntermediacao = linha.substring(190, 203);
					String tarifaDeIntermediacaoo = linha.substring(203, 216);
					String tarifaBoletoVendedor = linha.substring(216, 229);
					String repasseAplicacao = linha.substring(229, 242);
					String valorLiquidoDatransacao = linha.substring(242, 255);
					String statusDopagamento = linha.substring(255, 257);
					String filler = linha.substring(257, 259);
					String meioDepagamento = linha.substring(259, 261);
					String instituicaoFinanceira = linha.substring(261, 291);
					String canalDeEntrada = linha.substring(291, 293);
					String leitor = linha.substring(293, 295);
					String meioDecaptura = linha.substring(295, 297);
					String numerologico = linha.substring(297, 329);
					String nsu = linha.substring(329, 340);
					String filler1 = linha.substring(340, 343);
					String cartaoBin = linha.substring(343, 349);
					String cartaoHolder = linha.substring(349, 353);
					String codigoAutorizacao = linha.substring(353, 359);
					String codigoDoCV = linha.substring(359, 391);
					String reservadoParaUsofuturo = linha.substring(391);

					int tipoDeRegistroInt = strToInt(tipoDeRegistro);
					int estabelecimentoInt = strToInt(estabelecimento);
					int dataInicialDaTransacaoInt = strToInt(dataInicialDaTransacao);
					int dataDoEventoInt = strToInt(datadoEvento);
					int horaDoEventoInt = strToInt(horaDoEvento);
					int tipoDeEventoInt = strToInt(tipoDeEvento);
					int tipoDeTransacaoInt = strToInt(tipoDeTransacao);
					int valorTotalDaTransacaoInt = strToInt(valorTotalDaTransacao);
					int valorDaParcelaOuLiquidoTotalInt = strToInt(valorDaParcelaOuLiquidoTotal);
					int quantidaeDeParcelasDaTransacaoInt = strToInt(quantidaeDeParcelasDaTransacao);
					int dataPrevistaDePagamentoInt = strToInt(dataPrevistaDePagamento);
					int taxaDeParcelamentoCompradorInt = strToInt(taxaDeParcelamentoComprador);
					int tarifaBoletoCompradorInt = strToInt(tarifaBoletoComprador);
					int valorOriginalDaTransacaoInt = strToInt(valorOriginalDaTransacao);
					int taxaDeParcelamentoVendedorInt = strToInt(taxaDeParcelamentoVendedor);
					int taxaDeIntermediacaoInt = strToInt(taxaDeIntermediacao);
					int tarifaDeIntermediacaooInt = strToInt(tarifaDeIntermediacaoo);
					int tarifaBoletoVendedorInt = strToInt(tarifaBoletoVendedor);
					int repasseAplicacaoInt = strToInt(repasseAplicacao);
					int valorLiquidoDatransacaoInt = strToInt(valorLiquidoDatransacao);
					int statusDopagamentoInt = strToInt(statusDopagamento);
					int meioDepagamentoInt = strToInt(meioDepagamento);
					int leitorInt = strToInt(leitor);
					int meioDecapturaInt = strToInt(meioDecaptura);

					System.out.printf(tipoDeRegistroInt + " " + estabelecimentoInt + " " + dataInicialDaTransacaoInt
							+ " " + dataDoEventoInt + " " + horaDoEventoInt + " " + tipoDeEventoInt + " "
							+ tipoDeTransacaoInt + " " + numeroDeSerieDeLeitor + " " + codigoDaTransacao + " "
							+ codigoDoPedido + " " + valorDaParcelaOuLiquidoTotalInt + " " + pagamento + " " + plano
							+ " " + parcela + " " + quantidaeDeParcelasDaTransacaoInt + " " + dataPrevistaDePagamentoInt
							+ " " + taxaDeParcelamentoCompradorInt + " " + tarifaBoletoCompradorInt + " "
							+ valorOriginalDaTransacaoInt + " " + taxaDeParcelamentoVendedorInt + " "
							+ taxaDeIntermediacaoInt + " " + tarifaDeIntermediacaooInt + " " + tarifaBoletoVendedorInt
							+ " " + repasseAplicacaoInt + " " + repasseAplicacaoInt + " " + valorLiquidoDatransacaoInt
							+ " " + statusDopagamentoInt + " " + filler + " " + meioDepagamentoInt + " "
							+ instituicaoFinanceira + " " + canalDeEntrada + " " + leitorInt + " " + meioDecapturaInt
							+ " " + numerologico + " " + nsu + " " + filler1 + " " + cartaoBin + " " + cartaoHolder
							+ " " + codigoAutorizacao + " " + codigoDoCV + " " + reservadoParaUsofuturo + "\n");

				}
				
				if (linha.substring(0, 1).equals("9")) {
					String tipoDeRegistro = linha.substring(0, 1);
					String totalDeRegistro = linha.substring(1, 12);
					String reservadoParaUsosFuturos = linha.substring(12);

					int tipoDeRegistroInt = strToInt(tipoDeRegistro);
					int totalDeRegistroInt = strToInt(totalDeRegistro);

					System.out.printf(tipoDeRegistro + " " + totalDeRegistro + " " + reservadoParaUsosFuturos);
				}

			});
		} catch (IOException e) {
			System.out.println("Erro");
		}

	}

	public static int strToInt(String valor) {
		return Integer.parseInt(valor);
	}

}
